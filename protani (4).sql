-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 05, 2017 at 07:34 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `protani`
--

-- --------------------------------------------------------

--
-- Table structure for table `jenis_komentar`
--

CREATE TABLE `jenis_komentar` (
  `id` int(5) NOT NULL,
  `nama` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_komentar`
--

INSERT INTO `jenis_komentar` (`id`, `nama`) VALUES
(1, 'hama'),
(2, 'penyakit');

-- --------------------------------------------------------

--
-- Table structure for table `komentar`
--

CREATE TABLE `komentar` (
  `id` int(5) NOT NULL,
  `id_tanaman` int(5) NOT NULL,
  `id_jenis_komentar` int(5) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `gejala` text NOT NULL,
  `perawatan` text NOT NULL,
  `email_pengirim` varchar(100) NOT NULL,
  `nama_pengirim` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `komentar`
--

INSERT INTO `komentar` (`id`, `id_tanaman`, `id_jenis_komentar`, `nama`, `gejala`, `perawatan`, `email_pengirim`, `nama_pengirim`, `status`) VALUES
(5, 9, 1, 'Ulat', 'Terlihat lubang berjalan, dan sedikit idak sedap dipandang', 'Kurang penyemprotan pestisida', 'ndladuk@paijo.com', 'Ulat', 1),
(6, 3, 2, 'Kuning', 'Terlihat ada warna kuning di daun nya, sehingga tidak sedap dipandang jika akan dijadikan lalapan', 'Sering  sering dijaga kebersihannya, biar tidak sampai terkena penyakit seperti itu', 'kenteng@gmail.com', 'Kenteng', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tanaman`
--

CREATE TABLE `tanaman` (
  `id` int(5) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tanaman`
--

INSERT INTO `tanaman` (`id`, `nama`, `deskripsi`, `status`) VALUES
(3, 'Kemangi', 'sejenis dedaunan yang enak buat lalap, selain itu juga bisa ditemui dipinggir jalan', 1),
(7, 'Kolbis', 'Makanan lalap yang sangat terkenal di seluruh jagat dunia. Tanaman ini sering disajikan mentah dengan ikan bakar dan es teh manis', 0),
(9, 'Pisang', 'Tanaman kuning yang sangat laku di tanjung duren', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(5) NOT NULL,
  `name` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `password`) VALUES
(1, 'admin', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jenis_komentar`
--
ALTER TABLE `jenis_komentar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `komentar`
--
ALTER TABLE `komentar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_hama` (`id_jenis_komentar`),
  ADD KEY `id_tanaman` (`id_tanaman`);

--
-- Indexes for table `tanaman`
--
ALTER TABLE `tanaman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jenis_komentar`
--
ALTER TABLE `jenis_komentar`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `komentar`
--
ALTER TABLE `komentar`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tanaman`
--
ALTER TABLE `tanaman`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `komentar`
--
ALTER TABLE `komentar`
  ADD CONSTRAINT `komentar_ibfk_2` FOREIGN KEY (`id_jenis_komentar`) REFERENCES `jenis_komentar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `komentar_tanaman` FOREIGN KEY (`id_tanaman`) REFERENCES `tanaman` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
