<?php
/**
 * File ini berfungsi untuk menghapus data pada tabel tanaman
 */

 //untuk mengimpor file koneksi yang digunakan untuk berhubungan dengan database pada file ini
 include '../koneksi.php';
 
 //inisiasi array untuk hasil json
$resultArray = array();
$resultArray['data'] = [''];

//untuk mendapatkan id dari android melalui method POST http
$id = $_POST['id'];

//query untuk menghapus data sesuai id dari tabel tanaman
$query = "DELETE FROM tanaman WHERE id = " .  $id;

//eksekusi query menggunakan method mysqli_query
$result = mysqli_query($conn, $query);
//untuk cek apakah query berhasil di eksekusi atau tidak
if ($result) {
    //field status dengan value success ketika mysqli_query berhasil di eksekusi
    $resultArray['status'] = "success";
} else {
    //field status dengan value failed ketika mysqli_query gagal di eksekusi
    $resultArray['status'] = "failed";
}

//untuk menampilkan hasil berupa array yg sudah di inisiasi dari proses diatas lalu di decode ke json
echo json_encode($resultArray);
?>