<?php
/**
 * File ini berfungsi untuk menyimpan data pada tabel user
 */

 //untuk mengimpor file koneksi yang digunakan untuk berhubungan dengan database pada file ini
 include '../koneksi.php';
 
 //inisiasi array untuk hasil json
$resultArray = array();
$resultArray['data'][] = '';

//untuk mendapatkan username dari android melalui method POST http
$name = $_POST['username'];
//untuk mendapatkan password dari android melalui method POST http
$password = $_POST['password'];

//query untuk menyimpan data ke tabel user
$query = "INSERT INTO user(name,password) VALUES ("
        . "'" . $name . "',"
        . "'" . $password . "');";

//eksekusi query menggunakan method mysqli_query
$result = mysqli_query($conn, $query);
//untuk cek apakah query berhasil di eksekusi atau tidak
if ($result) {
    //field status dengan value success ketika mysqli_query berhasil di eksekusi
    $resultArray['status'] = "success";
} else {
    //field status dengan value failed ketika mysqli_query gagal di eksekusi
    $resultArray['status'] = "failed";
}

//untuk menampilkan hasil berupa array yg sudah di inisiasi dari proses diatas lalu di decode ke json
echo json_encode($resultArray);
?>