<?php
/**
 * File ini berfungsi untuk mengambil data (login, dsb) pada tabel user
 */

 //untuk mengimpor file koneksi yang digunakan untuk berhubungan dengan database pada file ini
 include '../koneksi.php';
 
 //inisiasi array untuk hasil json
$resultArray = array();
$resultArray['data'][] = '';

//query untuk mengambil data ke tabel user
$query = "SELECT * FROM user";

//cek jika user mengirim id dari android melalui method POST http
if (isset($_GET['id'])) {
    //query untuk mengambil data ke tabel user berdasarkan id
    $query = $query . " WHERE id = ". $_GET['id'];
//cek jika user mengirim name dan password dari android melalui method POST http
} else if (isset($_POST['name']) && isset($_POST['password'])) {
    //query untuk mengambil data ke tabel user berdasarkan username dan password
    $query = $query . " WHERE name = '". $_POST['name'] . "' AND password = '" . $_POST['password'] . "'";
}

//eksekusi query menggunakan method mysqli_query
$result = mysqli_query($conn, $query);
//untuk cek apakah query berhasil di eksekusi atau tidak, dan apakah ada data yang didapatkan dari eksekusi tersebut
if ($result && mysqli_num_rows($result) == 1) {
    //inisiasi array kembali untuk hasil json jika data berhasil didapatkan
	$resultArray = array();
    //untuk looping data yang didapatkan dari eksekusi query
    while ($row = mysqli_fetch_array($result)) {
        //inisiasi array untuk wadah data
        $resultData = array();
        //set field id pada array hasil
        $resultData['id'] = $row['id'];
        //set field name pada array hasil
        $resultData['name'] = $row['name'];
        //penambahan data yang ditambahkan pada array untuk hasil json
        $resultArray['data'][] = $resultData;
    }

    //field status dengan value success ketika mysqli_query berhasil di eksekusi
    $resultArray['status'] = "success";
} else {
    //field status dengan value failed ketika mysqli_query gagal di eksekusi
    $resultArray['status'] = "failed";
}

//untuk menampilkan hasil berupa array yg sudah di inisiasi dari proses diatas lalu di decode ke json
echo json_encode($resultArray);
?>