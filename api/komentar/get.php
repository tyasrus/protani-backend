<?php
/**
 * File ini berfungsi untuk mengambil data pada tabel komentar
 */

 //untuk mengimpor file koneksi yang digunakan untuk berhubungan dengan database pada file ini
 include '../koneksi.php';
 
 //inisiasi array untuk hasil json
$resultArray = array();
$resultArray['data'][] = '';

//query untuk mengambil data ke tabel komentar, tabel tanaman dengan menggunakan INNER JOIN berdasarkan id
$query = "SELECT komentar.*, tanaman.nama AS nama_tanaman, jenis_komentar.nama AS nama_jenis ".
"FROM komentar INNER JOIN tanaman ON komentar.id_tanaman = tanaman.id ".
"INNER JOIN jenis_komentar ON jenis_komentar.id = komentar.id_jenis_komentar ".
"WHERE komentar.id = " . $_GET['id'];

//eksekusi query menggunakan method mysqli_query
$result = mysqli_query($conn, $query);
//untuk cek apakah query berhasil di eksekusi atau tidak, dan apakah ada data yang didapatkan dari eksekusi tersebut
if ($result && mysqli_num_rows($result) == 1) {
    //inisiasi array kembali untuk hasil json jika data berhasil didapatkan
    $resultArray = array();
    //untuk looping data yang didapatkan dari eksekusi query
    while ($row = mysqli_fetch_array($result)) {
        //inisiasi array untuk wadah data
        $resultData = array();
        //set field id pada array hasil
        $resultData['id'] = $row['id'];
        //set field id_jenis_komentar pada array hasil
        $resultData['id_jenis_komentar'] = $row['id_jenis_komentar'];
        //set field nama pada array hasil
        $resultData['nama'] = $row['nama'];
        //set field gejala pada array hasil
        $resultData['gejala'] = $row['gejala'];
        //set field perawatan pada array hasil
        $resultData['perawatan'] = $row['perawatan'];
        //set field nama_pengirim pada array hasil
        $resultData['nama_pengirim'] = $row['nama_pengirim'];
        //set field email_pengirim pada array hasil
        $resultData['email_pengirim'] = $row['email_pengirim'];
        //set field id pada object tanaman pada array hasil
        $resultData['tanaman']['id'] = $row['id_tanaman'];
        //set field nama pada object tanaman pada array hasil
        $resultData['tanaman']['nama'] = $row['nama_tanaman'];
        //set field id pada object jenis pada array hasil
        $resultData['jenis']['id'] = $row['id_jenis_komentar'];
        //set field nama pada object jenis pada array hasil
        $resultData['jenis']['nama'] = $row['nama_jenis'];
        //set field status pada array hasil
        $resultData['status'] = $row['status'] == 0 ? false : true;
        //penambahan data yang ditambahkan pada array untuk hasil json
        $resultArray['data'][] = $resultData;
    }

    //field status dengan value success ketika mysqli_query berhasil di eksekusi
    $resultArray['status'] = "success";
} else {
    //field status dengan value failed ketika mysqli_query gagal di eksekusi
    $resultArray['status'] = "failed";
}

//untuk menampilkan hasil berupa array yg sudah di inisiasi dari proses diatas lalu di decode ke json
echo json_encode($resultArray);
?>