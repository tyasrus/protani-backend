<?php
/**
 * File ini berfungsi untuk mengambil semua data pada tabel jenis_komentar
 */

 //untuk mengimpor file koneksi yang digunakan untuk berhubungan dengan database pada file ini
include '../../koneksi.php';

//inisiasi array untuk hasil json
$resultArray = array();
$resultArray['data'][] = "";

//query untuk mengambil semua data dari tabel jenis_komentar
$query = "SELECT * FROM jenis_komentar";

//eksekusi query menggunakan method mysqli_query
$result = mysqli_query($conn, $query);
//untuk cek apakah query berhasil di eksekusi atau tidak
if ($result) {
    //untuk mengecek apakah ada data yang didapatkan dari eksekusi tersebut
    if (mysqli_num_rows($result) > 0) {
        //inisiasi array kembali untuk hasil json jika query berhasil di eksekusi
        $resultArray = array();
        //set field rows dari jumlah data yang didapatkan
        $resultArray['rows'] = mysqli_num_rows($result);
        //untuk looping data yang didapatkan dari eksekusi query
        while ($row = mysqli_fetch_array($result)) {
            //inisiasi array untuk wadah data
            $resultData = array();
            //set field id pada array hasil
            $resultData['id'] = $row['id'];
            //set field nama pada array hasil
            $resultData['nama'] = $row['nama'];
            //penambahan data yang ditambahkan pada array untuk hasil json
            $resultArray['data'][] = $resultData;
        }
    } else {
        //set field rows menjadi 0 karena tidak ada data yang didapatkan
        $resultArray['rows'] = 0;
    }

    //field status dengan value success ketika mysqli_query berhasil di eksekusi
    $resultArray['status'] = "success";
} else {
    //field status dengan value failed ketika mysqli_query gagal di eksekusi
    $resultArray['status'] = "failed";
}

//untuk menampilkan hasil berupa array yg sudah di inisiasi dari proses diatas lalu di decode ke json
echo json_encode($resultArray);
?>
