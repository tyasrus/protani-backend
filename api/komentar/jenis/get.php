<?php
/**
 * File ini berfungsi untuk mengambil data pada tabel jenis_komentar
 */

 //untuk mengimpor file koneksi yang digunakan untuk berhubungan dengan database pada file ini
include '../../koneksi.php';

//inisiasi array untuk hasil json
$resultArray = array();
$resultArray['data'][] = "";

//query untuk mengambil data sesuai id dari tabel jenis_komentar
$query = "SELECT * FROM jenis_komentar WHERE id = ". $_GET['id'];

//eksekusi query menggunakan method mysqli_query
$result = mysqli_query($conn, $query);
//untuk cek apakah query berhasil di eksekusi atau tidak, dan apakah ada data yang didapatkan dari eksekusi tersebut
if ($result && mysqli_num_rows($result) == 1) {
    //inisiasi array kembali untuk hasil json jika data berhasil didapatkan
    $resultArray = array();
    //untuk looping data yang didapatkan dari eksekusi query
    while ($row = mysqli_fetch_array($result)) {
        //inisiasi array untuk wadah data
        $resultData = array();
        //set field nama pada array hasil
        $resultData['nama'] = $row['nama'];
        //penambahan data yang ditambahkan pada array untuk hasil json
        $resultArray['data'][] = $resultData;
    }

    //field status dengan value success ketika mysqli_query berhasil di eksekusi
    $resultArray['status'] = "success";
} else {
    //field status dengan value failed ketika mysqli_query gagal di eksekusi
    $resultArray['status'] = "failed";
}

//untuk menampilkan hasil berupa array yg sudah di inisiasi dari proses diatas lalu di decode ke json
echo json_encode($resultArray);
?>
