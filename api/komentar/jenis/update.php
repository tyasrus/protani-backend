<?php
/**
 * File ini berfungsi untuk memperbarui data pada tabel jenis_komentar
 */

 //untuk mengimpor file koneksi yang digunakan untuk berhubungan dengan database pada file ini
 include '../../koneksi.php';
 
 //inisiasi array untuk hasil json
$resultArray = array();
$resultArray['data'][] = '';

//untuk mendapatkan id dari android melalui method POST http
$id = $_POST['id'];
//untuk mendapatkan nama dari android melalui method POST http
$nama = $_POST['nama'];

//query untuk memperbarui data ke tabel jenis_tanaman
$query = "UPDATE jenis_komentar SET nama = '" . $nama . "' WHERE id = " .  $id;

//eksekusi query menggunakan method mysqli_query
$result = mysqli_query($conn, $query);
//untuk cek apakah query berhasil di eksekusi atau tidak
if ($result) {
    //field status dengan value success ketika mysqli_query berhasil di eksekusi
    $resultArray['status'] = "success";
} else {
    //field status dengan value failed ketika mysqli_query gagal di eksekusi
    $resultArray['status'] = "failed";
}

//untuk menampilkan hasil berupa array yg sudah di inisiasi dari proses diatas lalu di decode ke json
echo json_encode($resultArray);
?>