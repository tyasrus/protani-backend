<?php
/**
 * File ini berfungsi untuk mengambil semua data pada tabel komentar
 */

 //untuk mengimpor file koneksi yang digunakan untuk berhubungan dengan database pada file ini
 include '../koneksi.php';
 
 //inisiasi array untuk hasil json
$resultArray = array();
$resultArray['data'] = [];

//query untuk mengambil data ke tabel komentar, tabel tanaman dengan menggunakan INNER JOIN
$query = "SELECT komentar.*, tanaman.nama AS nama_tanaman, jenis_komentar.nama AS nama_jenis ".
"FROM komentar INNER JOIN tanaman ON komentar.id_tanaman = tanaman.id ".
"INNER JOIN jenis_komentar ON jenis_komentar.id = komentar.id_jenis_komentar";

//query untuk mengambil data ke tabel komentar, tabel tanaman dengan menggunakan INNER JOIN berdasarkan id_tanaman dan id_jenis_komentar
if (isset($_GET['id_tanaman']) && isset($_GET['id_jenis_komentar'])) {
    $query = "SELECT komentar.*, tanaman.nama AS nama_tanaman, jenis_komentar.nama AS nama_jenis ".
    "FROM komentar INNER JOIN tanaman ON komentar.id_tanaman = tanaman.id ".
    "INNER JOIN jenis_komentar ON jenis_komentar.id = komentar.id_jenis_komentar ".
    "WHERE komentar.id_tanaman = " . $_GET['id_tanaman'] . " AND komentar.id_jenis_komentar = " . $_GET['id_jenis_komentar'] . " AND komentar.status = true";;
}

//eksekusi query menggunakan method mysqli_query
$result = mysqli_query($conn, $query);
//untuk cek apakah query berhasil di eksekusi atau tidak
if ($result) {
    //untuk mengecek apakah ada data yang didapatkan dari eksekusi tersebut
    if (mysqli_num_rows($result) > 0) {
        //inisiasi array kembali untuk hasil json jika query berhasil di eksekusi
        $resultArray = array();
        //set field rows dari jumlah data yang didapatkan
        $resultArray['rows'] = mysqli_num_rows($result);
        //untuk looping data yang didapatkan dari eksekusi query
        while ($row = mysqli_fetch_array($result)) {
            //inisiasi array untuk wadah data
            $resultData = array();
            //set field id pada array hasil
            $resultData['id'] = $row['id'];
            //set field id_jenis_komentar pada array hasil
            $resultData['id_jenis_komentar'] = $row['id_jenis_komentar'];
            //set field nama pada array hasil
            $resultData['nama'] = $row['nama'];
            //set field gejala pada array hasil
            $resultData['gejala'] = $row['gejala'];
            //set field perawatan pada array hasil
            $resultData['perawatan'] = $row['perawatan'];
            //set field nama_pengirim pada array hasil
            $resultData['nama_pengirim'] = $row['nama_pengirim'];
            //set field email_pengirim pada array hasil
            $resultData['email_pengirim'] = $row['email_pengirim'];
            //set field id pada object tanaman pada array hasil
            $resultData['tanaman']['id'] = $row['id_tanaman'];
            //set field nama pada object tanaman pada array hasil
            $resultData['tanaman']['nama'] = $row['nama_tanaman'];
            //set field id pada object jenis pada array hasil
            $resultData['jenis']['id'] = $row['id_jenis_komentar'];
            //set field nama pada object jenis pada array hasil
            $resultData['jenis']['nama'] = $row['nama_jenis'];
            //set field status pada array hasil
            $resultData['status'] = $row['status'] == 0 ? false : true;
            //penambahan data yang ditambahkan pada array untuk hasil json
            $resultArray['data'][] = $resultData;
        }
    } else {
        //set field rows menjadi 0 karena tidak ada data yang didapatkan
        $resultArray['rows'] = 0;
    }
    
    //field status dengan value success ketika mysqli_query berhasil di eksekusi
    $resultArray['status'] = "success";
} else {
    //field status dengan value failed ketika mysqli_query gagal di eksekusi
    $resultArray['status'] = "failed";
}

//untuk menampilkan hasil berupa array yg sudah di inisiasi dari proses diatas lalu di decode ke json
echo json_encode($resultArray);
?>