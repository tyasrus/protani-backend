<?php
/**
 * File ini berfungsi untuk memperbarui data pada tabel komentar
 */

 //menggunakan modul PHPMailer dari library menggunakan keyword use
use PHPMailer\PHPMailer\PHPMailer;
//menggunakan modul Exception dari library menggunakan keyword use
use PHPMailer\PHPMailer\Exception;

//untuk mengimpor file autoload yang digunakan untuk pengiriman email pada file ini
require '../../vendor/autoload.php';

 //untuk mengimpor file koneksi yang digunakan untuk berhubungan dengan database pada file ini
include '../koneksi.php';

//inisiasi array untuk hasil json
$resultArray = array();
$resultArray['data'][] = '';

//untuk mendapatkan id dari android melalui method POST http
$id = $_POST['id'];
//untuk mendapatkan id_jenis_komentar dari android melalui method POST http
$idJenisKomentar = $_POST['id_jenis_komentar'];
//untuk mendapatkan id_tanaman dari android melalui method POST http
$idTanaman = $_POST['id_tanaman'];
//untuk mendapatkan nama dari android melalui method POST http
$nama = $_POST['nama'];
//untuk mendapatkan gejala dari android melalui method POST http
$gejala = $_POST['gejala'];
//untuk mendapatkan perawatan dari android melalui method POST http
$perawatan = $_POST['perawatan'];
//untuk mendapatkan nama_pengirim dari android melalui method POST http
$namaPengirim = $_POST['nama_pengirim'];
//untuk mendapatkan email_pengirim dari android melalui method POST http
$emailPengirim = $_POST['email_pengirim'];
//untuk mendapatkan status dari android melalui method POST http
$status = $_POST['status'];

    //untuk mendapatkan status dari tabel komentar berdasarkan id
    $query = "SELECT status FROM komentar WHERE id = " .  $id;
    //eksekusi query menggunakan method mysqli_query
    $result = mysqli_query($conn, $query);
    //untuk cek apakah query berhasil di eksekusi atau tidak, dan apakah ada data yang didapatkan dari eksekusi tersebut
    if ($result && mysqli_num_rows($result) == 1) {
        //untuk looping data yang didapatkan dari eksekusi query
        while ($row = mysqli_fetch_array($result)) {      
            //set variabel oldStatus untuk menampung status yang sudah ada pada tabel komentar
            $oldStatus = $row['status'];
        }

        //query untuk memperbarui data ke tabel komentar
        $query = "UPDATE komentar SET id_tanaman = " . $idTanaman . ",".
        "id_jenis_komentar = " . $idJenisKomentar . ",". 
        "nama = '" . $nama . "',". 
        "gejala = '" . $gejala . "',".
        "perawatan = '" . $perawatan . "',".
        "nama_pengirim = '" . $namaPengirim . "',".
        "email_pengirim = '" . $emailPengirim . "',".
        "status = " . $status . " WHERE id = " .  $id;
        //eksekusi query menggunakan method mysqli_query
        $result = mysqli_query($conn, $query);
        // echo "old - " . $oldStatus . ", new - " . $status;
        //untuk cek apakah query berhasil di eksekusi atau tidak
        if ($result) {
        //cek kondisi jika variabel oldStatus = false dan variabel status = true dari android
            if ($oldStatus == 0 && $status == 'true') {
                //inisiasi variabel mail untuk pengiriman email
                //Passing `true` enables exceptions
                $mail = new PHPMailer(true);                              
                try {
                    //untuk nyalain debug
                    // Enable verbose debug output
                    $mail->SMTPDebug = 3;                                 
                    // Set mailer to use SMTP
                    $mail->isSMTP();                 
                    // Specify main and backup SMTP servers                     
                    $mail->Host = 'smtp.gmail.com';  
                    // Enable SMTP authentication
                    $mail->SMTPAuth = true;                               
                    // SMTP username
                    $mail->Username = 'protani.irvandha@gmail.com';               
                    // SMTP password  
                    $mail->Password = 'p4ijoNdl4Duck!';                   
                    // Enable TLS encryption, `ssl` also accepted        
                    $mail->SMTPSecure = 'tls';                            
                    // TCP port to connect to
                    $mail->Port = 587;                                    
                    
                    //untuk mengatur identitas pengirim email
                    $mail->setFrom('irvandha@protani.com', 'Irvandha, CEO ProTani');
                    //untuk mengatur identitas penerima email
                    $mail->addAddress($emailPengirim);              
                    //untuk mengatur identitas email jika penerima ingin membalas email
                    $mail->addReplyTo('protani.irvandha@gmail.com', 'Feedback');
                    
                    //untuk mengatur jika konten yang dikirim dalam format html
                    $mail->isHTML(true);
                    //untuk mengatur judul subject email
                    $mail->Subject = 'Selamat! Komentar Anda telah di Verifikasi!';
                    //untuk mengatur isi email dalam bentuk html
                    $mail->Body    = '<h3>Hai ' . $namaPengirim . '</h3><br/><br/>Beberapa waktu yang lalu kami menerima komentar tentang gangguan atau hama ' . $nama . '. Setelah kami melihat dan mereview komentar anda, kami rasa komentar anda sangat berguna bagi pengguna kami yang lain. Kami sangat menghargai kontribusi anda. <br/><br/><br/></br></br>Regards,<br/><b>Irvandha (CEO ProTani)</b>';
                    
                    //untuk mengirim email
                    $mail->send();
                    //field message dengan value email terkirim! ketika email berhasil dikirim
                    $resultArray['message'] = "email terkirim!";
                } catch (Exception $e) {
                    //field message akan menampilkan error message ketika email gagal dikirim
                    $resultArray['message'] = "email error  - " . $mail->ErrorInfo;
                }
            }
        } else {
            //field status dengan value failed ketika mysqli_query gagal di eksekusi
            $resultArray['status'] = "failed";
        }

        //field status dengan value success ketika mysqli_query berhasil di eksekusi
        $resultArray['status'] = "success";
    } else {
        //field status dengan value failed ketika mysqli_query gagal di eksekusi
        $resultArray['status'] = "failed";
    }
    


//untuk menampilkan hasil berupa array yg sudah di inisiasi dari proses diatas lalu di decode ke json
echo json_encode($resultArray);
?>