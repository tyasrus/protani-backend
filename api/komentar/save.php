<?php
/**
 * File ini berfungsi untuk menyimpan data pada tabel komentar
 */

 //untuk mengimpor file koneksi yang digunakan untuk berhubungan dengan database pada file ini
 include '../koneksi.php';
 
 //inisiasi array untuk hasil json
$resultArray = array();
$resultArray['data'][] = '';

//untuk mendapatkan id_jenis_komentar dari android melalui method POST http
$idJenisKomentar = $_POST['id_jenis_komentar'];
//untuk mendapatkan id_tanaman dari android melalui method POST http
$idTanaman = $_POST['id_tanaman'];
//untuk mendapatkan nama dari android melalui method POST http
$nama = $_POST['nama'];
//untuk mendapatkan gejala dari android melalui method POST http
$gejala = $_POST['gejala'];
//untuk mendapatkan perawatan dari android melalui method POST http
$perawatan = $_POST['perawatan'];
//untuk mendapatkan nama_pengirim dari android melalui method POST http
$namaPengirim = $_POST['nama_pengirim'];
//untuk mendapatkan email_pengirim dari android melalui method POST http
$emailPengirim = $_POST['email_pengirim'];
//untuk mendapatkan status dari android melalui method POST http
$status = $_POST['status'];

//query untuk menyimpan data ke tabel komentar
$query = "INSERT INTO komentar(id_tanaman, id_jenis_komentar, nama, gejala, perawatan, nama_pengirim, email_pengirim, status) VALUES ("
        . "" . $idTanaman . ","
        . "" . $idJenisKomentar . ","
        . "'" . $nama . "',"
        . "'" . $gejala . "',"
        . "'" . $perawatan . "',"
        . "'" . $namaPengirim . "',"
        . "'" . $emailPengirim . "',"
        . "" . $status . ");";

//eksekusi query menggunakan method mysqli_query
$result = mysqli_query($conn, $query);
//untuk cek apakah query berhasil di eksekusi atau tidak
if ($result) {
    //field status dengan value success ketika mysqli_query berhasil di eksekusi
    $resultArray['status'] = "success";
} else {
    //field status dengan value failed ketika mysqli_query gagal di eksekusi
    $resultArray['status'] = "failed";
}

//untuk menampilkan hasil berupa array yg sudah di inisiasi dari proses diatas lalu di decode ke json
echo json_encode($resultArray);
?>